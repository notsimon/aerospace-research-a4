ASAR products
=============

[Products structure](https://earth.esa.int/handbooks/asar/CNTR2-3-2.htm)

p. 168 of the handbook

* MPH (Main Product Header)
* SPH (Specific Product Header)
    - DSD (Data Set Descriptor)
* Data Sets (DS)

General Software Tools (p. 136)

[ASAR products handbook](https://earth.esa.int/pub/ESA_DOC/ENVISAT/ASAR/asar.ProductHandbook.2_2.pdf)

File names
==========

Product file name convention identical for all Envisat instruments

ASA_IMS_1P|N|PDE|20070821_165527_000000172061_00012_28623_9672.N1

* ASA_IMS_1P = Product ID
    - ASAR Image Mode Single Look Complex Level 1P
* N = Processing stage flag
    - NRT product
* PDE = Originator ID / Processing Facility
    - PDHS-E
* 20070821 = Start day (YYYYMMDD) 21 Aug 2007
* 165527 = StartTime (16:55:27 UTC)
* 000000172061= Duration - (17 seconds) 2 (phase) 061 (cycle)
* 00012 = Relative orbit / track
* 28623 = Absolute orbit
* 9672 = Product type file counter
* N1 = Satellite ID
    - N1 = Envisat-1

[Envisat products file namming convention](http://earth.esa.int/pub/ESA_DOC/ENVISAT/AnnexA_Data_Convention_3e.pdf)

