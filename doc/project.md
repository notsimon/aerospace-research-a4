A4 project summary
==================

The goal of the project is to reorder and convert the data sets contained in the 
[EO Virtual Archive database](http://eo-virtual-archive4.esa.int/).

# Tasks

* Specify a portable (and flexible) output format
    - How the frames are organized
    - Data format (CVS, JSON, database etc.)
* Describe the raw data sets: mainly what are the 'nodes' in the various formats
* From a set of N1 files: reorder their data sets depending on the coordinates
    - Cut the product into frames:
        * map the start product to the range of frames covered using the
          start/stop times and the orbit start
        * compute the coordinates of the frame corners
    - Rearrange the frames depending on their coordinates
* Support of other formats than the N1

# Tech

* C++ for everything
* Boost Spirit for the N1 files parsing (done)

# Remarks / Questions

I'm not sure that the reordering process is very intensive and then justifies
the usage of a distributed computing platform.
It may be much less productive for a server to manage a BOINC task (that is, 
download the products from the database, organize the work-units, transfert the 
data to the peers, retrieve the results and put it back to the database) than a 
direct conversion... (this remark also applies to GPU-computing)

What is an orbit propagator ? Is the one from ESOC still available ?

# Docs

* [ASAR products handbook](https://earth.esa.int/pub/ESA_DOC/ENVISAT/ASAR/asar.ProductHandbook.2_2.pdf)
* [Envisat products file namming convention](http://earth.esa.int/pub/ESA_DOC/ENVISAT/AnnexA_Data_Convention_3e.pdf)

## ESA framing standard

* 1 orbit = 7200 nodes = 400 frames
* 1 ASAR/IMS frame = 18 nodes

Ex, first frame:
* Range = nodes [0,18[
* Center = ID = node 9
