import sys
import os

APPNAME = 'a4'
VERSION = '0.1'

top = '.'
out = 'build'

def options(ctx):
    ctx.load('compiler_cxx')

    ctx.add_option(
        '-m', '--mode',
        action  = 'store',
        default = 'debug',
        help    = 'build mode (release, debug or profile)'
    )

def configure(ctx):
    ctx.load('compiler_cxx')

    if 'C_INCLUDE_PATH' in os.environ:
        ctx.env.append_value('INCLUDES', os.environ['C_INCLUDE_PATH'].split(':'))

    if 'LD_LIBRARY_PATH' in os.environ:
        ctx.env.append_value('LIBPATH', os.environ['LD_LIBRARY_PATH'].split(':'))

    ctx.check(lib = 'boost_system-mt')
    ctx.check(lib = 'boost_filesystem-mt')
    ctx.check(lib = 'boost_regex-mt')

def build(ctx):
    cxxflags = {
        'release': ['-O4', '-W', '-DNDEBUG'],
        'debug': ['-O0', '-W', '-Wall', '-g']
    }

    ctx.env.append_unique('CXXFLAGS', cxxflags[ctx.options.mode])
    ctx.env.append_unique('CXXFLAGS', "-std=c++11")
    #ctx.env.append_unique('CXXFLAGS', "-fdiagnostics-color=always")

    ctx.program(
        target      = 'convert',
        lib         = ['boost_filesystem-mt', 'boost_system-mt', 'boost_regex-mt'],
        source      = ctx.path.ant_glob(['src/*.cc'])
    )
