#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/get.hpp>
#include <string>
#include <fstream>
#include <iostream>

typedef unsigned int uint;

namespace spirit = boost::spirit;
namespace qi = spirit::qi;
namespace phoenix = boost::phoenix;

typedef boost::variant<std::string, double, long> Value;

struct Field {
    std::string key;
    Value value;
};

BOOST_FUSION_ADAPT_STRUCT(
    Field,
    (std::string, key)
    (Value, value)
)

template <typename Iterator>
struct ProductGrammar : qi::grammar<Iterator, std::vector<Field>()> {
    ProductGrammar();

    qi::rule<Iterator, std::vector<Field>()> product;
    qi::rule<Iterator, Field()> field;
    qi::rule<Iterator, std::string()> key;
    qi::rule<Iterator, Value()> value;
    qi::rule<Iterator, std::string()> quoted_string;
    qi::rule<Iterator, boost::variant<double, long>()> numeric;
    qi::rule<Iterator> unit;
};

template <typename Iterator>
ProductGrammar<Iterator>::ProductGrammar()
    : ProductGrammar::base_type(product) {

    using qi::char_;

    product = field % (qi::eol >> *qi::space);

    field %= key >> '=' > value;
    key %= +(char_ - '=' - qi::eol);

    value %= quoted_string
          | +qi::alpha
          | numeric;

    quoted_string %= qi::lexeme[char_('"') >> *(char_ - '"') >> '"'];

    numeric %= ((qi::long_ >> (!char_('.')))
             | qi::double_)
            >> -unit;

    unit = char_('<') >> +(char_ - '>') >> '>';

#define NAME(R) R.name(#R)
    NAME(product);
    NAME(field);
    NAME(key);
    NAME(value);
    NAME(quoted_string);
    NAME(numeric);
    NAME(unit);
#undef NAME

#ifndef NDEBUG
    using namespace qi::labels;

    //debug(product);
    debug(field);
    //debug(key);
    debug(value);
    debug(numeric);
#endif
}

int main(int argc, char** argv) {
    if (argc < 2)
        return 1;

    ProductGrammar<spirit::istream_iterator> product_grammar;

    std::ifstream in(argv[1]);
    in.unsetf(std::ios::skipws);

    spirit::istream_iterator begin(in);
    spirit::istream_iterator end;

    std::vector<Field> product;

    bool parsed = qi::phrase_parse(begin, end,
                                   product_grammar,
                                   qi::space - qi::eol,
                                   product);

    if (!parsed) {
        std::cerr << "An error occured while parsing the product header" << std::endl;
        return 2;
    }

    for (const auto& f : product) {
        std::cout << f.key << " = " << f.value << "; ";
    }

    std::cout << std::endl;

    // find number of data sets
    long num_data_sets = 0;
    for (const auto& f : product)
        if (f.key == "NUM_DATA_SETS") {
            num_data_sets = boost::get<long>(f.value);
            break;
        }

    if (num_data_sets == 0) {
        std::cerr << "No data sets found in this product" << std::endl;
        return 2;
    }

    std::cout << num_data_sets << " data sets found" << std::endl;

    return 0;
}
